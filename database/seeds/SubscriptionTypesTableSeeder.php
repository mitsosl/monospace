<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SubscriptionTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('subscription_types')->insert([
           'name' => "Lite",
           'price' => 15000
        ]);

        DB::table('subscription_types')->insert([
            'name' => "Basic",
            'price' => 20000
        ]);

        DB::table('subscription_types')->insert([
            'name' => "Pro",
            'price' => 30000
        ]);
    }
}
