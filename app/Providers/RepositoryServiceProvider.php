<?php

namespace App\Providers;

use App\Repositories\Interfaces\MonospaceApiRepositoryInterface;
use App\Repositories\Interfaces\SubscriptionRepositoryInterface;
use App\Repositories\Interfaces\SubscriptionTypeRepositoryInterface;
use App\Repositories\MonospaceApiRepository;
use App\Repositories\SubscriptionRepository;
use App\Repositories\SubscriptionTypeRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            MonospaceApiRepositoryInterface::class,
            MonospaceApiRepository::class
        );
        $this->app->bind(
            SubscriptionRepositoryInterface::class,
            SubscriptionRepository::class
        );
        $this->app->bind(
            SubscriptionTypeRepositoryInterface::class,
            SubscriptionTypeRepository::class
        );
    }
}
