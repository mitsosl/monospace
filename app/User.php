<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * @param $user
     *
     * @return bool
     */
    public static function isUserValid($user): bool
    {
        if (false === isset($user)) return false;

        if (is_null($user)) return false;

        if(
            false === isset($user->active) ||
            false === isset($user->name) ||
            false === isset($user->email) ||
            false === isset($user->phone) ||
            false === isset($user->type) ||
            false === isset($user->id)
        ) {
            return false;
        }

        return true;
    }

    public static function isUserActive($user): bool
    {
        if (false === self::isUserValid($user)) return false;

        return true === $user->active;
    }
}
