<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    protected $fillable = [
        'name',
        'price',
        'from',
        'to'
    ];

    protected $dates = [
        'from',
        'to'
    ];

    /**
     * @param $user
     * @param $price
     *
     * @return Subscription
     */
    public static function createForUserAndPrice($user, $price)
    {
        $newSubscriptionForUser = new Subscription();
        $newSubscriptionForUser->external_user_id = $user->id;
        $newSubscriptionForUser->price = $price;
        $newSubscriptionForUser->from = Carbon::now();
        $newSubscriptionForUser->to = Carbon::now()->addMonths(6);

        return $newSubscriptionForUser;
    }

    public function subscriptionType() {
        return $this->belongsTo(SubscriptionType::class);
    }
}
