<?php

namespace App\Http\Controllers;

use App\Repositories\Interfaces\MonospaceApiRepositoryInterface;
use App\Repositories\Interfaces\SubscriptionRepositoryInterface;
use App\Repositories\Interfaces\SubscriptionTypeRepositoryInterface;
use App\Subscription;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ApiSubscriptionController extends Controller
{
    /**
     * @var SubscriptionRepositoryInterface
     */
    private $subscriptionRepository;

    /**
     * @var SubscriptionTypeRepositoryInterface
     */
    private $subscriptionTypeRepository;

    /**
     * @var MonospaceApiRepositoryInterface
     */
    private $monospaceApiRepository;

    public function __construct(
        SubscriptionRepositoryInterface $subscriptionRepository,
        SubscriptionTypeRepositoryInterface $subscriptionTypeRepository,
        MonospaceApiRepositoryInterface $monospaceApiRepository
    ) {
        $this->subscriptionRepository = $subscriptionRepository;
        $this->subscriptionTypeRepository = $subscriptionTypeRepository;
        $this->monospaceApiRepository = $monospaceApiRepository;
    }

    /**
     * Needs Validation / Combined filters / Refactor to remove code from controller / Constants etc...
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $subscriptions = [];

        if (false === is_null(request()->active)){
            if (request()->active === "1") {
                $subscriptions = $this->subscriptionRepository->getActiveSubscriptions();
            } else if (request()->active === "0") {
                $subscriptions = $this->subscriptionRepository->getPastSubscriptions();
            }
        } else if (false === is_null(request()->from) && false === is_null(request()->to)) {
            $subscriptions = $this->subscriptionRepository->getSubscriptionsBetweenDates(
                request()->from,
                request()->to
            );
        }

        return response()->json(
            [
                'subscriptions' => $subscriptions
            ],
            200
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return JsonResponse
     */
    public function store(): JsonResponse
    {
        $user = $this->monospaceApiRepository->getOneUser();
        $randomSubscriptionType = $this->subscriptionTypeRepository->getOneRandom();

        $subscription = $this->subscriptionRepository
            ->createSubscriptionForUserIfNeeded($user, $randomSubscriptionType);

        return response()->json(
            [
                'subscription' => $subscription
            ],
            200
        );
    }
}
