<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubscriptionType extends Model
{
    const SUBSCRIPTION_TYPES_EQUAL = 0;
    const SUBSCRIPTION_TYPES_FIRST_SUPERIOR = -1;
    const SUBSCRIPTION_TYPES_SECOND_SUPERIOR = 1;

    protected $fillable = [
        'name',
        'price'
    ];

    /**
     * @param SubscriptionType $one
     * @param SubscriptionType $two
     *
     * @return int
     */
    public static function compareSubscriptionTypes(SubscriptionType $one, SubscriptionType $two)
    {
        if ($one->price === $two->price) return self::SUBSCRIPTION_TYPES_EQUAL;

        if ($one->price > $two->price) return self::SUBSCRIPTION_TYPES_FIRST_SUPERIOR;

        return self::SUBSCRIPTION_TYPES_SECOND_SUPERIOR;
    }
}
