<?php


namespace App\Repositories\Interfaces;


interface MonospaceApiRepositoryInterface
{
    public function getOneUser();
}
