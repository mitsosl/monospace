<?php


namespace App\Repositories\Interfaces;


use App\SubscriptionType;

interface SubscriptionRepositoryInterface
{
    public function all();

    public function getActiveSubscriptions();

    public function getPastSubscriptions();

    public function getSubscriptionsBetweenDates($from, $to);

    public function createSubscriptionForUserIfNeeded($user, SubscriptionType $subscriptionType);
}
