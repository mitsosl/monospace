<?php


namespace App\Repositories\Interfaces;


interface SubscriptionTypeRepositoryInterface
{
    public function getOneRandom();
}
