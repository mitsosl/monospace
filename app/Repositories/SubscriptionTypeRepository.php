<?php


namespace App\Repositories;


use App\Repositories\Interfaces\SubscriptionTypeRepositoryInterface;
use App\SubscriptionType;

class SubscriptionTypeRepository implements SubscriptionTypeRepositoryInterface
{

    public function getOneRandom()
    {
        return SubscriptionType::all()->random()->first();
    }
}
