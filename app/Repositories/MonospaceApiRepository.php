<?php


namespace App\Repositories;


use App\Repositories\Interfaces\MonospaceApiRepositoryInterface;

class MonospaceApiRepository implements MonospaceApiRepositoryInterface
{
    const API_USERS_URL = "http://liapis.front.challenge.dev.monospacelabs.com/users";

    public function getOneUser()
    {
        $users = $this->getUsersAsArray();

        $randomKey = array_rand($users, 1);
        $oneUser = $users[$randomKey];

        return $oneUser;
    }

    protected function getUsersAsArray()
    {
        $response = file_get_contents(self::API_USERS_URL, false);

        return json_decode($response);
    }
}
