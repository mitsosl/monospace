<?php


namespace App\Repositories;


use App\Repositories\Interfaces\SubscriptionRepositoryInterface;
use App\Subscription;
use App\SubscriptionType;
use App\User;
use Carbon\Carbon;

class SubscriptionRepository implements SubscriptionRepositoryInterface
{

    public function all()
    {
        return Subscription::all();
    }

    public function getActiveSubscriptions()
    {
        return Subscription::where([
            ['from', '<=', Carbon::now()->toDateString()],
            ['to', '>=', Carbon::now()->toDateString()]
        ])->get();
    }

    public function getPastSubscriptions()
    {
        return Subscription::where(
            'to', '<=', Carbon::now()->toDateString()
        )->get();
    }

    public function getSubscriptionsBetweenDates($from, $to)
    {
        return Subscription::where([
            ['from', '>=', $from],
            ['to', '<=', $to]
        ])->get();
    }

    /**
     * @param $user
     * @param SubscriptionType $subscriptionType
     *
     * @return Subscription | null
     */
    public function createSubscriptionForUserIfNeeded($user, SubscriptionType $subscriptionType): ? Subscription
    {
        if (false === User::isUserActive($user)) return null;

        $preExistingSubscriptionForUser = Subscription::where([
            ['external_user_id', '=', $user->id],
            ['from', '<=', Carbon::now()],
            ['to', '>=', Carbon::now()]
        ])->first();

        $newSubscriptionForUser = null;

        if ($preExistingSubscriptionForUser instanceof Subscription) {
            if (SubscriptionType::SUBSCRIPTION_TYPES_SECOND_SUPERIOR === SubscriptionType::compareSubscriptionTypes(
                    $preExistingSubscriptionForUser->subscriptionType(),
                    $subscriptionType
            )) {
                $preExistingSubscriptionForUser->to = Carbon::now();
                $preExistingSubscriptionForUser->save();

                $price = $subscriptionType->price * 0.7;

                $newSubscriptionForUser = Subscription::createForUserAndPrice($user, $price);

                $newSubscriptionForUser->subscriptionType()->associate($subscriptionType);
                $newSubscriptionForUser->save();
            }
        } else {
            $price = $subscriptionType->price;

            $newSubscriptionForUser = Subscription::createForUserAndPrice($user, $price);

            $newSubscriptionForUser->subscriptionType()->associate($subscriptionType);
            $newSubscriptionForUser->save();
        }

        return $newSubscriptionForUser;
    }
}
