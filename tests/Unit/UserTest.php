<?php

namespace Tests\Unit;

use App\User;
use Tests\TestCase;

class UserTest extends TestCase
{
    const VALID_ACTIVE_USER =
      '{
        "active": true,
        "name": "Colleen Barrows",
        "email": "Ricardo_Hilll@gmail.com",
        "phone": "031-316-8089",
        "type": "Guest",
        "id": "5dc5881568cf2f7f3adea36f"
      }';

    const VALID_INACTIVE_USER =
        '{
        "active": false,
        "name": "Colleen Barrows",
        "email": "Ricardo_Hilll@gmail.com",
        "phone": "031-316-8089",
        "type": "Guest",
        "id": "5dc5881568cf2f7f3adea36f"
      }';

    const USER_MISSING_ID =
        '{
        "active": true,
        "name": "Colleen Barrows",
        "email": "Ricardo_Hilll@gmail.com",
        "phone": "031-316-8089",
        "type": "Guest",
      }';

    const USER_MISSING_STATUS =
        '{
        "name": "Colleen Barrows",
        "email": "Ricardo_Hilll@gmail.com",
        "phone": "031-316-8089",
        "type": "Guest",
        "id": "5dc5881568cf2f7f3adea36f"
      }';

    const EMPTY_USER = '';

    /**
     * @return void
     */
    public function testIsUserValidSucceedsForValidUser()
    {
        $user = json_decode(self::VALID_ACTIVE_USER);

        $this->assertTrue(User::isUserValid($user));
    }

    /**
     * @return void
     */
    public function testIsUserValidFailsForUserMissingStatus()
    {
        $user = json_decode(self::USER_MISSING_STATUS);

        $this->assertFalse(User::isUserValid($user));
    }

    /**
     * @return void
     */
    public function testIsUserValidFailsForUserMissingId()
    {
        $user = json_decode(self::USER_MISSING_ID);

        $this->assertFalse(User::isUserValid($user));
    }

    /**
     * @return void
     */
    public function testIsUserValidFailsForEmptyUser()
    {
        $user = json_decode(self::USER_MISSING_ID);

        $this->assertFalse(User::isUserValid($user));
    }

    /**
     * @return void
     */
    public function testIsUserActiveSucceedsForActiveUser()
    {
        $user = json_decode(self::VALID_ACTIVE_USER);

        $this->assertTrue(User::isUserActive($user));
    }

    /**
     * @return void
     */
    public function testIsUserActiveFailsForInactiveUser()
    {
        $user = json_decode(self::VALID_INACTIVE_USER);

        $this->assertFalse(User::isUserActive($user));
    }
}
