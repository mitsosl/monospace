<?php


namespace Tests\Unit;


use App\SubscriptionType;
use Tests\TestCase;

class SubscriptionTypeTest extends TestCase
{
    public function testThatCompareSubscriptionTypesSucceedsForFirstSuperior()
    {
        $oneSubscriptionType = new SubscriptionType();
        $oneSubscriptionType->name = "one";
        $oneSubscriptionType->price = 15000;

        $secondSubscriptionType = new SubscriptionType();
        $secondSubscriptionType->name = "two";
        $secondSubscriptionType->price = 17000;

        $this->assertEquals(
            SubscriptionType::SUBSCRIPTION_TYPES_FIRST_SUPERIOR,
            SubscriptionType::compareSubscriptionTypes($secondSubscriptionType, $oneSubscriptionType)
        );
    }

    public function testThatCompareSubscriptionTypesSucceedsForSecondSuperior()
    {
        $oneSubscriptionType = new SubscriptionType();
        $oneSubscriptionType->name = "one";
        $oneSubscriptionType->price = 15000;

        $secondSubscriptionType = new SubscriptionType();
        $secondSubscriptionType->name = "two";
        $secondSubscriptionType->price = 17000;

        $this->assertEquals(
            SubscriptionType::SUBSCRIPTION_TYPES_SECOND_SUPERIOR,
            SubscriptionType::compareSubscriptionTypes($oneSubscriptionType, $secondSubscriptionType)
        );
    }

    public function testThatCompareSubscriptionTypesSucceedsForEquals()
    {
        $oneSubscriptionType = new SubscriptionType();
        $oneSubscriptionType->name = "one";
        $oneSubscriptionType->price = 17000;

        $secondSubscriptionType = new SubscriptionType();
        $secondSubscriptionType->name = "two";
        $secondSubscriptionType->price = 17000;

        $this->assertEquals(
            SubscriptionType::SUBSCRIPTION_TYPES_EQUAL,
            SubscriptionType::compareSubscriptionTypes($secondSubscriptionType, $oneSubscriptionType)
        );
    }
}
